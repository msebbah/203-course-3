from squared_range import run

assert run(4) == [0, 1, 4, 9]

print('coverage: 25% of statements')